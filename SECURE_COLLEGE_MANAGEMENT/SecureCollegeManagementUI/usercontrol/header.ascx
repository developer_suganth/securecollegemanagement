﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header.ascx.cs" Inherits="SecureCollegeManagementUI.usercontrol.header" %>
<link href="<%=ResolveUrl("~/fontawesome/css/font-awesome.min.css")%>" rel="stylesheet" />
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="javascript:;" id="TxtCollegeName" style="color: white" onclick="home()"><i class="fa fa-home"></i>&nbsp;SRI KRISHNA ENGINEERING COLLEGE</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:;" onclick="logout();">Logout</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<script src="<%=ResolveUrl("~/script/jquery-2.0.3.min.js")%>"></script>
<script type="text/javascript">
    function logout() {
        var pageURL = $(location).attr("href").split('setup');
        if (pageURL.length == 2) {
            location.href = "../../default.aspx";
        } else {
            location.href = "../default.aspx";
        }
    }

    function home() {
        var pageURL = $(location).attr("href").split('setup');
        if (pageURL.length == 2) {
            location.href = "../Landing.aspx";
        } else {
            location.href = "Landing.aspx";
        }
    }
</script>