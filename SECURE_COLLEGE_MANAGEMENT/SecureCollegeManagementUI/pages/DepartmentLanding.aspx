﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DepartmentLanding.aspx.cs" Inherits="SecureCollegeManagementUI.pages.DepartmentLanding" %>

<%@ Register Src="~/usercontrol/header.ascx" TagPrefix="uc1" TagName="header" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Department Landing</title>
    <link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bootstrap/custom.css" rel="stylesheet" />
    <style>
        .btn-default:active, .btn-default.active, .open .dropdown-toggle.btn-default {
            background: #4299d8 !important;
            text-shadow: none !important;
            color: white !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <uc1:header runat="server" ID="header" />
            <section>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h4>Students</h4>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Joining Year</label>
                                    <div class="input-group">
                                        <select class="form-control" id="DrpJoiningYear">
                                            <option value="0" selected="selected">--select--</option>
                                            <option value="2012">2012</option>
                                            <option value="2013">2013</option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                        <span class="input-group-btn">
                                            <input type="button" class="btn btn-default" value="View" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Reg. No.</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.</td>
                                                <td>1042424125</td>
                                                <td>Name1</td>
                                            </tr>
                                            <tr>
                                                <td>2.</td>
                                                <td>1042424125</td>
                                                <td>Name2</td>
                                            </tr>
                                            <tr>
                                                <td>3.</td>
                                                <td>1042424125</td>
                                                <td>Name3</td>
                                            </tr>
                                            <tr>
                                                <td>4.</td>
                                                <td>1042424125</td>
                                                <td>Name4</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>

                         </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h4>Exams Mark Entry</h4>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="">Semester:</label>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="btn-group" data-toggle="buttons" id="DivSemesters">
                                    <label class="btn btn-default" onclick="SemClick(1);">
                                        <input type="radio" name="options" id="option1" autocomplete="off">
                                        1
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(2);">
                                        <input type="radio" name="options" id="option2" autocomplete="off">
                                        2
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(3);">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        3
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(4);">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        4
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(5);">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        5
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(6);">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        6
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(7);">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        7
                                    </label>
                                    <label class="btn btn-default" onclick="SemClick(8);">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        8
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="help-block" />
                        <div class="row" id="DivExams" style="display: none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="">Exam:</label>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default" onclick="ExamClick(this)">
                                        <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                        Exam 1
                                    </label>
                                    <label class="btn btn-default" onclick="ExamClick(this)">
                                        <input type="radio" name="options" id="option2" autocomplete="off">
                                        Exam 2
                                    </label>
                                    <label class="btn btn-default" onclick="ExamClick(this)">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        Exam 3
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="help-block" />
                        <div class="row" id="DivSubjects" style="display: none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="">Subject:</label>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default" onclick="SubjectClick(this)">
                                        <input type="radio" name="options" id="option1" autocomplete="off" checked>
                                        Subject 1
                                    </label>
                                    <label class="btn btn-default" onclick="SubjectClick(this)">
                                        <input type="radio" name="options" id="option2" autocomplete="off">
                                        Subject 2
                                    </label>
                                    <label class="btn btn-default" onclick="SubjectClick(this)">
                                        <input type="radio" name="options" id="option3" autocomplete="off">
                                        Subject 3
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="DivProceed" style="display: none">
                            <hr />
                            <input type="button" class="btn btn-success" value="Proceed" onclick="ProceedToEntry();" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
    <script src="../script/jquery-2.0.3.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
    <script>

        function SemClick(sem) {
            $("#DivExams").css('display', 'block');
            $("#DivSubjects").css('display', 'none');
            $("#DivProceed").css('display', 'none')
        }
        function ExamClick(exam) {
            $("#DivSubjects").css('display', 'block');
            $("#DivProceed").css('display', 'none')
        }
        function SubjectClick(sub) {
            $("#DivProceed").css('display', 'block');
        }
        function ProceedToEntry() {
            location.href = "MarksEntry.aspx";
        }
    </script>
</body>
</html>
