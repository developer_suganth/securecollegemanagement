﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="SecureCollegeManagementUI.pages.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Password Recovery</title>
    <link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
     <style>
        @media(max-width:767px) {
            .jumbotron {
                padding: 30px !important;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-top: 100px">
            <div class="container">
                <header class="text-center">
                    <h2>SECURE COLLEGE MANAGEMENT</h2>
                </header>
                <hr />
                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <h4 class="text-center">Password Recovery</h4>
                                <div class="jumbotron">

                                    <div id="DivEmail">
                                        <div class="form-group">
                                            <label>Email ID</label>
                                            <input type="email" id="TxtUserName" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <input type="button" id="BtnSecurityQuestion" value="Get Security Question" class="btn btn-success" onclick="GetSecurityQstn();" />
                                        </div>
                                    </div>

                                    <div id="DivSecurityQstn" style="display:none">
                                        <div class="form-group">
                                            <label>What is your favorite color?</label>
                                            <input type="text" id="TxtAnswer" class="form-control" placeholder="Answer..." />
                                        </div>
                                        <div class="form-group">
                                            <input type="button" id="BtnGetPassword" value="Get Password" class="btn btn-success" onclick="GetPassword();" />
                                        </div>
                                    </div>

                                    <div id="DivPassword" style="display:none">
                                       Your password is: <h3>12345</h3>
                                    </div>
                                  

                                    <div class="form-group">
                                        <a href="../default.aspx">Cancel</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
    <script src="../script/jquery-2.0.3.min.js"></script>
    <script>
        function GetSecurityQstn() {
            $("#DivSecurityQstn").css("display","block");
            $("#DivEmail").css("display", "none");
        }
        function GetPassword() {
            $("#DivPassword").css("display", "block");
            $("#DivSecurityQstn").css("display", "none");
        }
    </script>
</body>
</html>
