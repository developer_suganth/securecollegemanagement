﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MarksEntry.aspx.cs" Inherits="SecureCollegeManagementUI.pages.MarksEntry" %>

<%@ Register Src="~/usercontrol/header.ascx" TagPrefix="uc1" TagName="header" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Marks Entry</title>
    <link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bootstrap/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <uc1:header runat="server" ID="header" />
            <section>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Joining Year</label>
                            <select class="form-control" id="DrpJoiningYear">
                                <option value="0" selected="selected">--select--</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Maximum Marks</label>
                            <input type="text" maxlength="3" id="TxtMaxMark" class="form-control" placeholder="eg: 100" />
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group">
                            <input type="button" class="btn btn-success" value="Save" />
                            <a href="DepartmentLanding.aspx" class="btn btn-danger">Cancel</a>
                        </div>


                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-bordered table-responsive table-hover">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Reg. No.</th>
                                    <th>Student Name</th>
                                    <th>Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>1042424125</td>
                                    <td>Name1</td>
                                    <td>
                                        <input type="text" maxlength="3" class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>1042424125</td>
                                    <td>Name2</td>
                                    <td>
                                        <input type="text" maxlength="3" class="form-control" /></td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>1042424125</td>
                                    <td>Name3</td>
                                    <td>
                                        <input type="text" maxlength="3" class="form-control" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </form>
    <script src="../script/jquery-2.0.3.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
</body>
</html>

