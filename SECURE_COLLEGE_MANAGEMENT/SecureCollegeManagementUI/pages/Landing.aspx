﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="SecureCollegeManagementUI.pages.Landing" %>

<%@ Register Src="~/usercontrol/header.ascx" TagPrefix="uc1" TagName="header" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome</title>
    <link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bootstrap/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">

                <uc1:header runat="server" ID="header" />

                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h4>Departments</h4>
                                <a href="DepartmentLanding.aspx" class="form-control btn btn-default custom-lnk">ECE - Electronics and Communication Engieering</a>
                                <a href="DepartmentLanding.aspx" class="form-control btn btn-default custom-lnk">CSE - Computer Science Engineering</a>
                                <a href="DepartmentLanding.aspx" class="form-control btn btn-default custom-lnk">IT - Information Techology</a>
                                <a href="DepartmentLanding.aspx" class="form-control btn btn-default custom-lnk">EEE - Electronics and Electrical Engineering</a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h4>Setup</h4>
                                <a href="setup/Users.aspx" class="form-control btn btn-default custom-lnk">Users</a>
                                <a href="setup/Department.aspx" class="form-control btn btn-default custom-lnk">Departments</a>
                                <a href="setup/Subjects.aspx" class="form-control btn btn-default custom-lnk">Subjects</a>
                                <a href="setup/Exams.aspx" class="form-control btn btn-default custom-lnk">Exams</a>
                                <a href="setup/CollegeProfile.aspx" class="form-control btn btn-default custom-lnk">College Profile</a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
    <script src="../script/jquery-2.0.3.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
</body>
</html>
