﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="SecureCollegeManagementUI.pages.setup.Users" %>

<%@ Register Src="~/usercontrol/header.ascx" TagPrefix="uc1" TagName="header" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Users</title>
    <link href="../../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../bootstrap/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <uc1:header runat="server" ID="header" />
               
                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-push-6 col-md-push-6 col-lg-6 col-md-6 col-md-12 col-xs-12">
                                <h4>User Entry</h4>
                                <hr />
                                <div class="form-group">
                                    <label>Department</label>
                                    <input type="text" id="TxtDept" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control" id="DrpType">
                                        <option value="0" selected="selected">--select--</option>
                                        <option value="1">Student</option>
                                        <option value="2">Staff</option>
                                    </select>
                                </div>
                                <div class="form-group" id="DivStaff" style="display: none">
                                    <label>Designation</label>
                                    <input type="text" id="TxtDesignation" class="form-control" />
                                </div>
                                <div id="DivStudent" style="display: none">
                                    <div class="form-group">
                                        <label>Joining Year</label>
                                        <select class="form-control" id="DrpJoiningYear">
                                            <option value="0" selected="selected">--select--</option>
                                            <option value="2012">2012</option>
                                            <option value="2013">2013</option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                        <small>Ending Year: 2016, Total Semester:8</small>
                                    </div>
                                    <div class="form-group">
                                        <label>Registration No</label>
                                        <input type="text" id="TxtRegNo" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" id="TxtName" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" id="TxtEmail" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" id="TxtPwd" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Security Question</label>
                                    <input type="text" id="TxtSecurityQstn" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Answer</label>
                                    <input type="text" id="TxtAnswer" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <input type="button" class="btn btn-success" value="Save" id="BtnSave" />
                                    <input type="button" class="btn btn-warning" value="Reset" id="BtnReset" />
                                </div>
                            </div>
                            <div class="col-lg-pull-6 col-md-pull-6 col-lg-6 col-md-6 col-md-12 col-xs-12">
                                <h4>User List</h4>
                                <hr />
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Department</th>
                                            <th>Type</th>
                                            <th>Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Name1</td>
                                            <td>Email1@gmail.com</td>
                                            <td>IT</td>
                                            <td>Staff</td>
                                            <td>
                                                <input type="button" class="btn btn-primary btn-xs" value="Edit" />
                                                <input type="button" class="btn btn-danger btn-xs" value="Delete" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Name2</td>
                                            <td>Email2@gmail.com</td>
                                            <td>CSE</td>
                                            <td>Student</td>
                                            <td>
                                                <input type="button" class="btn btn-primary btn-xs" value="Edit" />
                                                <input type="button" class="btn btn-danger btn-xs" value="Delete" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
    <script src="../../script/jquery-2.0.3.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.js"></script>
    <script>

        $("#DrpType").change(function () {
            if ($("#DrpType").val() == 1) {
                $("#DivStaff").hide();
                $("#DivStudent").show();
            } else if ($("#DrpType").val() == 2) {
                $("#DivStaff").show();
                $("#DivStudent").hide();
            } else {
                $("#DivStudent,#DivStaff").hide();
            }
        });

        $("#BtnReset").click(function () {
            $("#TxtDept,#TxtDesignation,#TxtRegNo,#TxtName,#TxtEmail,#TxtPwd,#TxtSecurityQstn,#TxtAnswer").val("");
            $("#DrpType,#DrpJoiningYear").val(0);
            $("#DrpType").change();
        });

    </script>
</body>
</html>
