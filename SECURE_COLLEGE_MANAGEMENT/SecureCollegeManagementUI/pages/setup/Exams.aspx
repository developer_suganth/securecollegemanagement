﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exams.aspx.cs" Inherits="SecureCollegeManagementUI.pages.setup.Exams" %>

<%@ Register Src="~/usercontrol/header.ascx" TagPrefix="uc1" TagName="header" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Exams</title>
    <link href="../../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../bootstrap/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <uc1:header runat="server" ID="header" />
               
                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-push-6 col-md-push-6 col-lg-6 col-md-6 col-md-12 col-xs-12">
                                <h4>Exam Entry</h4>
                                <hr />
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" id="TxtName" />
                                </div>
                                <div class="form-group">
                                    <label>Semesters</label>
                                    <select class="form-control" id="DrpSem">
                                        <option value="0" selected="selected">--select--</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="button" id="BtnSave" value="Save" class="btn btn-success" />
                                    <input type="button" id="BtnReset" value="Reset" class="btn btn-warning" />
                                </div>
                            </div>
                            <div class="col-lg-pull-6 col-md-pull-6 col-lg-6 col-md-6 col-md-12 col-xs-12">
                                <h4>Exam List</h4>
                                <hr />
                                <table class="table table-bordered table-responsive" id="TblExams">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Semester</th>
                                            <th>Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>UNIT 1</td>
                                            <td>8</td>
                                            <td>
                                                <input type="button" class="btn btn-primary btn-xs" value="Edit" />
                                                <input type="button" class="btn btn-danger btn-xs" value="Delete" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>UNIT 1</td>
                                            <td>8</td>
                                            <td>
                                                <input type="button" class="btn btn-primary btn-xs" value="Edit" />
                                                <input type="button" class="btn btn-danger btn-xs" value="Delete" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>UNIT 3</td>
                                            <td>4</td>
                                            <td>
                                                <input type="button" class="btn btn-primary btn-xs" value="Edit" />
                                                <input type="button" class="btn btn-danger btn-xs" value="Delete" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
     <script src="../../script/jquery-2.0.3.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("#BtnReset").click(function () {
            $("#TxtName").val("");
            $("#DrpSem").val(0);
        });
    </script>
</body>
</html>
