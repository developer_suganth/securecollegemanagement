﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CollegeProfile.aspx.cs" Inherits="SecureCollegeManagementUI.pages.setup.CollegeProfile" %>

<%@ Register Src="~/usercontrol/header.ascx" TagPrefix="uc1" TagName="header" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>College Profile</title>
    <link href="../../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../bootstrap/custom.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <uc1:header runat="server" ID="header" />
                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>College Name</label>
                                    <input type="text" id="TxtCollegeName" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" id="TxtAddress" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <input type="button" class="btn btn-success" value="Save" id="BtnSave"/>
                                    <a type="button" class="btn btn-danger" href="../Landing.aspx" >Cancel</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </form>
     <script src="../../script/jquery-2.0.3.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.js"></script>
</body>
</html>

