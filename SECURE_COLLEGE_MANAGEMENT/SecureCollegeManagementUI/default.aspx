﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SecureCollegeManagementUI._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Secure College Management</title>
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <style>
        @media(max-width:767px) {
            .jumbotron {
                padding: 30px !important;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-top:100px">
        <div class="container">
            <header class="text-center">
               <h2>SECURE COLLEGE MANAGEMENT</h2> 
            </header>
            <hr />
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="jumbotron">
                                <div class="form-group">
                                    <label>Email ID</label>
                                    <input type="email" id="TxtUserName" class="form-control"  />
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" id="TxtPassword" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <input type="button" id="BtnLogin" value="Login" class="btn btn-success" onclick="Login();" />
                                    <a href="pages/ForgotPassword.aspx">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </form>
    <script src="script/jquery-2.0.3.min.js"></script>
    <script src="default.js"></script>
</body>
</html>
